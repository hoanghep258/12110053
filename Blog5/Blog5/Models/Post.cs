﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog5.Models
{
    public class Post
    {
        public int ID { set; get; }
        public string Title { set; get; }
        public string Body { set; get; }
        [DataType(DataType.DateTime)]
        public DateTime DateCreate { set; get; }


        public virtual ICollection<Comment> Comments { set; get; }
        public virtual ICollection<Tag> Tags { set; get; }

    }
}