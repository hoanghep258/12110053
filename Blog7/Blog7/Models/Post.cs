﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Blog7.Models
{
    public class Post
    {
        public int ID { set; get; }
        public string Title { set; get; }
        public string Body { set; get; }
        public DateTime DateCreate { set; get; }

        public virtual UserProfile UserProfile { set; get; }
        public int UserProfileId { set; get; }
    }
}