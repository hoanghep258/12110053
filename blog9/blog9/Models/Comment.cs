﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace blog9.Models
{
    public class Comment
    {
        public int ID { set; get; }
        public string Body { set; get; }
        [DataType(DataType.DateTime)]
        public DateTime DateCreate { set; get; }


        public int LastTime
        {
            get
            {
                return (DateTime.Now -DateCreate).Minutes;
            }
        }
        public int PostID { set; get; }
        public virtual Post Post { set; get; }

    }
}