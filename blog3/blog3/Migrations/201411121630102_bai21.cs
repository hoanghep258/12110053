namespace blog3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class bai21 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Comments", "TagID", c => c.Int(nullable: false));
            AlterColumn("dbo.Tags", "Content", c => c.String());
            AddForeignKey("dbo.Comments", "TagID", "dbo.Tags", "TagID", cascadeDelete: true);
            CreateIndex("dbo.Comments", "TagID");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Comments", new[] { "TagID" });
            DropForeignKey("dbo.Comments", "TagID", "dbo.Tags");
            AlterColumn("dbo.Tags", "Content", c => c.String(maxLength: 100));
            DropColumn("dbo.Comments", "TagID");
        }
    }
}
