namespace blog3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class bai2 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Accounts",
                c => new
                    {
                        AccountID = c.Int(nullable: false, identity: true),
                        Password = c.String(),
                        FirstName = c.String(maxLength: 100),
                        LastName = c.String(maxLength: 100),
                        Email = c.String(),
                    })
                .PrimaryKey(t => t.AccountID);
            
            CreateTable(
                "dbo.Tags",
                c => new
                    {
                        TagID = c.Int(nullable: false, identity: true),
                        Content = c.String(maxLength: 100),
                    })
                .PrimaryKey(t => t.TagID);
            
            CreateTable(
                "dbo.AccountPosts",
                c => new
                    {
                        Account_AccountID = c.Int(nullable: false),
                        Post_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Account_AccountID, t.Post_ID })
                .ForeignKey("dbo.Accounts", t => t.Account_AccountID, cascadeDelete: true)
                .ForeignKey("dbo.Posts", t => t.Post_ID, cascadeDelete: true)
                .Index(t => t.Account_AccountID)
                .Index(t => t.Post_ID);
            
            CreateTable(
                "dbo.TagPosts",
                c => new
                    {
                        Tag_TagID = c.Int(nullable: false),
                        Post_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Tag_TagID, t.Post_ID })
                .ForeignKey("dbo.Tags", t => t.Tag_TagID, cascadeDelete: true)
                .ForeignKey("dbo.Posts", t => t.Post_ID, cascadeDelete: true)
                .Index(t => t.Tag_TagID)
                .Index(t => t.Post_ID);
            
            AddColumn("dbo.Posts", "AccountID", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropIndex("dbo.TagPosts", new[] { "Post_ID" });
            DropIndex("dbo.TagPosts", new[] { "Tag_TagID" });
            DropIndex("dbo.AccountPosts", new[] { "Post_ID" });
            DropIndex("dbo.AccountPosts", new[] { "Account_AccountID" });
            DropForeignKey("dbo.TagPosts", "Post_ID", "dbo.Posts");
            DropForeignKey("dbo.TagPosts", "Tag_TagID", "dbo.Tags");
            DropForeignKey("dbo.AccountPosts", "Post_ID", "dbo.Posts");
            DropForeignKey("dbo.AccountPosts", "Account_AccountID", "dbo.Accounts");
            DropColumn("dbo.Posts", "AccountID");
            DropTable("dbo.TagPosts");
            DropTable("dbo.AccountPosts");
            DropTable("dbo.Tags");
            DropTable("dbo.Accounts");
        }
    }
}
