﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace blog3.Models
{
   
    public class Post
    {
        public int ID { set; get; }
        public string Title { set; get; }
        public string Body { set; get; }
        [DataType(DataType.DateTime)]
        public DateTime DateCreate { set; get; }
        public virtual ICollection<Comment> Comments { set; get; }
        public int AccountID { get; set; }

        public virtual ICollection<Account> Accounts { get; set; }
        public virtual ICollection<Tag> Tags { get; set; }
    }
}