﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace blog3.Models
{
    public partial class Tag
    {
        public Tag()
        {
            this.Posts = new HashSet<Post>();
        }
        [Required]
        public int TagID { get; set; }
        
        public string Content { get; set; }

        public virtual ICollection<Post> Posts { get; set; }
    }
}