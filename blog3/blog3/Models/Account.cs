﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace blog3.Models
{
    public class Account
    {
        public Account()
        {
            this.Posts = new HashSet<Post>();
        }

        [Required]
        public int AccountID { get; set; }
        [DataType(DataType.Password)]
        public string Password { get; set; }
        [StringLength(100, ErrorMessage = "Không nhập quá 100 ký tự")]
        public string FirstName { get; set; }
        [StringLength(100, ErrorMessage = "Không nhập quá 100 ký tự")]
        public string LastName { get; set; }
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        public virtual ICollection<Post> Posts { get; set; }
    }
}